<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz - P2</title>
    <link rel="icon" type="image/x-icon" href="#">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>
    <?php
    session_start();
    $QUESTIONS = array(
        1 => array(
            'Q' => "What does PHP stand for?",
            1 => "PHP: Hypertext Preprocessor",
            2 => "Personal Hypertext Processor",
            3 => "Private Home Page",
            4 => "Others",
            "A" => 1
        ),
        2 => array(
            'Q' => "PHP server scripts are surrounded by delimiters, which?",
            1 => htmlspecialchars("<?php> ... </?>"),
            2 => htmlspecialchars("<&> ... </&>"),
            3 => htmlspecialchars("<?php ... ?>"),
            4 => htmlspecialchars("<script> ... </script>"),
            "A" => 3
        ),
        3 => array(
            'Q' => "How do you write \"Hello World\" in PHP",
            1 => "echo \"Hello World\";",
            2 => "Document.Write(\"Hello World\");",
            3 => "\"Hello World\";",
            4 => "println(\"Hello World\");",
            "A" => 1
        ),
        4 => array(
            'Q' => "All variables in PHP start with which symbol?",
            1 => "_",
            2 => "!",
            3 => "&",
            4 => "$",
            "A" => 4
        ),
        5 => array(
            'Q' => "What is the correct way to end a PHP statement?",
            1 => ";",
            2 => ".",
            3 => "newline",
            4 => htmlspecialchars("</php>"),
            "A" => 1
        ),
        6 => array(
            'Q' => "The PHP syntax is most similar to:",
            1 => "Javascript",
            2 => "Perl and C",
            3 => "VBScript",
            4 => "Lua",
            "A" => 2
        ),
        7 => array(
            'Q' => "How do you get information from a form that is submitted using the \"get\" method?",
            1 => "\$_GET[];",
            2 => "Request.QueryString;",
            3 => "Request.Form;",
            4 => "Both 1 and 3",
            "A" => 1
        ),
        8 => array(
            'Q' => "What is the correct way to include the file \"time.inc\" ?",
            1 => htmlspecialchars("<?php include file=\"time.inc\"; ?>"),
            2 => htmlspecialchars("<?php include:\"time.inc\"; ?>"),
            3 => htmlspecialchars("<!-- include file=\"time.inc\" -->"),
            4 => htmlspecialchars("<?php include \"time.inc\"; ?>"),
            "A" => 4
        ),
        8 => array(
            'Q' => "What is the correct way to open the file \"time.txt\" as readable?",
            1 => "fopen(\"time.txt\",\"r+\");",
            2 => "open(\"time.txt\",\"read\");",
            3 => "fopen(\"time.txt\",\"r\");",
            4 => "open(\"time.txt\");",
            "A" => 3
        ),
        9 => array(
            'Q' => "Which super global variable holds information about headers, paths, and script locations?",
            1 => "\$_SESSION",
            2 => "\$_GLOBALS",
            3 => "\$_SERVER",
            4 => "\$_GET",
            "A" => 3
        ),
        10 => array(
            'Q' => "What is the correct way to add 1 to the \$count variable?",
            1 => "\$count++;",
            2 => "++count",
            3 => "\$count =+1",
            4 => "count++;",
            "A" => 1
        ),
    );
    ?>
    <section class="section-content padding-y">
        <form method="POST">
            <div class="card mx-auto p-3" style="max-width: 50%; margin-top:100px;">
                <div class="card-body">
                    <?php
                    function save()
                    {
                        global $QUESTIONS;
                        for ($i = 6; $i <= 10; $i++) {
                            if (isset($_POST["q-$i"])) {
                                if ($_POST["q-$i"] == $QUESTIONS[$i]['A']) {
                                    $_SESSION['score'] = $_SESSION['score'] + 1;
                                }
                                setcookie("q-$i", $_POST["q-$i"], time() + 86400, "/");
                            }
                        }
                        echo "<script>console.log(\"". $_SESSION['score'] ."\")</script>";
                        header("Location: ./score.php");
                    }

                    function navigate($idx, $len)
                    {
                        setcookie("start", ($idx - 1) * 5 + 1, time() + 86400, '/');
                        setcookie("end", $idx * 5, time() + 86400, '/');
                        if ($_COOKIE['end'] > $len) {
                            setcookie("end", $len, time() + 86400, '/');
                        }
                    }

                    function isChecked($i, $value)
                    {
                        if (isset($_COOKIE["q-$i"]) and $_COOKIE["q-$i"] == $value) {
                            echo " checked";
                        }
                    }

                    if ($_SERVER["REQUEST_METHOD"] == "POST" and isset($_POST["navigation"])) {
                        // $idx = $_POST["navigation"];
                        save();
                        // navigate($idx, count($QUESTIONS));
                    }

                    for ($i = 6; $i <= 10; $i++) {
                    ?>
                        <div class="mb-4">
                            <div class="row">
                                <h5 class="col"><?= $QUESTIONS[$i]["Q"] ?></h5>
                            </div>
                            <div class="row mb-2">
                                <div class="col">
                                    <input class="form-check-input me-2" type="radio" name="<?= "q-$i" ?>" id="<?= "q-$i-1" ?>" value="1" <?php isChecked($i, 1) ?>>
                                    <label class="form-check-label" for="<?= "q-$i-1" ?>">
                                        <?= $QUESTIONS[$i][1] ?>
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input me-2" type="radio" name="<?= "q-$i" ?>" id="<?= "q-$i-2" ?>" value="2" <?php isChecked($i, 2) ?>>
                                    <label class="form-check-label" for="<?= "q-$i-2" ?>">
                                        <?= $QUESTIONS[$i][2] ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input class="form-check-input me-2" type="radio" name="<?= "q-$i" ?>" id="<?= "q-$i-3" ?>" value="3" <?php isChecked($i, 3) ?>>
                                    <label class="form-check-label" for="<?= "q-$i-3" ?>">
                                        <?= $QUESTIONS[$i][3] ?>
                                    </label>
                                </div>
                                <div class="col">
                                    <input class="form-check-input me-2" type="radio" name="<?= "q-$i" ?>" id="<?= "q-$i-4" ?>" value="4" <?php isChecked($i, 4) ?>>
                                    <label class="form-check-label" for="<?= "q-$i-4" ?>">
                                        <?= $QUESTIONS[$i][4] ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <nav aria-label="Form Navigation">
                <ul class="pagination justify-content-center mt-3">
                    <li class="page-item">
                        <button class="page-link" type="submit" name="<?= "navigation" ?>" value="<?= $i ?>"><a href="index.php">1</a></button>
                    </li>
                    <li class="page-item">
                        <button class="page-link" type="submit" name="<?= "navigation" ?>" value="<?= $i ?>">2</button>
                    </li>
                    <li class="page-item">
                        <button class="page-link" type="submit" name="<?= "navigation" ?>" value="<?= $i ?>">Nộp bài</button>
                    </li>
                </ul>
            </nav>
        </form>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

</body>

</html>