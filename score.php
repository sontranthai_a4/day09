<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz - Score</title>
    <link rel="icon" type="image/x-icon" href="#">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
</head>

<body>
    <?php
    session_start();
    $QUESTIONS = array(
        1 => array(
            'Q' => "What does PHP stand for?",
            1 => "PHP: Hypertext Preprocessor",
            2 => "Personal Hypertext Processor",
            3 => "Private Home Page",
            4 => "Others",
            "A" => 1
        ),
        2 => array(
            'Q' => "PHP server scripts are surrounded by delimiters, which?",
            1 => htmlspecialchars("<?php> ... </?>"),
            2 => htmlspecialchars("<&> ... </&>"),
            3 => htmlspecialchars("<?php ... ?>"),
            4 => htmlspecialchars("<script> ... </script>"),
            "A" => 3
        ),
        3 => array(
            'Q' => "How do you write \"Hello World\" in PHP",
            1 => "echo \"Hello World\";",
            2 => "Document.Write(\"Hello World\");",
            3 => "\"Hello World\";",
            4 => "println(\"Hello World\");",
            "A" => 1
        ),
        4 => array(
            'Q' => "All variables in PHP start with which symbol?",
            1 => "_",
            2 => "!",
            3 => "&",
            4 => "$",
            "A" => 4
        ),
        5 => array(
            'Q' => "What is the correct way to end a PHP statement?",
            1 => ";",
            2 => ".",
            3 => "newline",
            4 => htmlspecialchars("</php>"),
            "A" => 1
        ),
        6 => array(
            'Q' => "The PHP syntax is most similar to:",
            1 => "Javascript",
            2 => "Perl and C",
            3 => "VBScript",
            4 => "Lua",
            "A" => 2
        ),
        7 => array(
            'Q' => "How do you get information from a form that is submitted using the \"get\" method?",
            1 => "\$_GET[];",
            2 => "Request.QueryString;",
            3 => "Request.Form;",
            4 => "Both 1 and 3",
            "A" => 1
        ),
        8 => array(
            'Q' => "What is the correct way to include the file \"time.inc\" ?",
            1 => htmlspecialchars("<?php include file=\"time.inc\"; ?>"),
            2 => htmlspecialchars("<?php include:\"time.inc\"; ?>"),
            3 => htmlspecialchars("<!-- include file=\"time.inc\" -->"),
            4 => htmlspecialchars("<?php include \"time.inc\"; ?>"),
            "A" => 4
        ),
        8 => array(
            'Q' => "What is the correct way to open the file \"time.txt\" as readable?",
            1 => "fopen(\"time.txt\",\"r+\");",
            2 => "open(\"time.txt\",\"read\");",
            3 => "fopen(\"time.txt\",\"r\");",
            4 => "open(\"time.txt\");",
            "A" => 3
        ),
        9 => array(
            'Q' => "Which super global variable holds information about headers, paths, and script locations?",
            1 => "\$_SESSION",
            2 => "\$_GLOBALS",
            3 => "\$_SERVER",
            4 => "\$_GET",
            "A" => 3
        ),
        10 => array(
            'Q' => "What is the correct way to add 1 to the \$count variable?",
            1 => "\$count++;",
            2 => "++count",
            3 => "\$count =+1",
            4 => "count++;",
            "A" => 1
        ),
    );
    ?>
    <section class="section-content padding-y">
        <div class="card mx-auto p-3" style="max-width: 50%; margin-top:100px;">
            <div class="card-body">
                <?php
                $score = $_SESSION['score'];
                echo "<h5>Điểm của bạn là : $score</h5>";
                if ($score < 4) {
                    echo "<h5>Bạn quá kém, cần ôn tập thêm</h5>";
                } else if ($score <= 7) {
                    echo "<h5>Cũng bình thường</h5>";
                } else {
                    echo "<h5>Sắp sửa làm được trợ giảng lớp PHP</h5>";
                }
                ?>
            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

</body>

</html>